import { GET_CARD_DATA } from "./types";
import axios from "axios";

  
  export const fetchScoreData = () => (dispatch) => { // transaction list
    axios.get(`https://backend.sports.info/api/v1/nba/game/f34b1dfd-97fd-4942-9c14-05a05eeb5921/summary`).then((res) => {
      console.log("data", res)
      dispatch({
        type: GET_CARD_DATA,
        payload: res.data.data,
      });
    })
    .catch((error) => console.log(error));
  }

