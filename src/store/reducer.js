import { GET_CARD_DATA } from "./types";

export default (state = {}, action) => {
    console.log("Payload", action.payload)

  switch (action.type) {
    case GET_CARD_DATA:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
};



