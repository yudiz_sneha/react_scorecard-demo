import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { fetchScoreData } from "../store/action";
import './ScoreCard.css';

const ScoreCard = () => {

 
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchScoreData());
    console.log("Data")
  }, []);
  const scoredata = useSelector(state => state);
  console.log("Data", scoredata);

  return (
    <div className="container">
      <div className="row">
        <div style={{ textAlign: "left" }}>
          <img src="https://media.sports.info/logos/sr:team:3420.png" />
            <h4>{scoredata.card.data.home.market} {scoredata.card.data.home.name}</h4>
            <h2>{scoredata.card.data.home.points}</h2>
        </div>

        <div>
          <h3 style={{ textAlign: "right" }}>{scoredata.card.data.venue.name}, {scoredata.card.data.venue.city} | 19 April 2022</h3>
        </div>
      </div>
    </div>
  );
};

export default ScoreCard;
