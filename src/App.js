import React from 'react';
import ScoreCard from './components/ScoreCard'
import './App.css';

function App() {
  return (
    <div className="App">
      <ScoreCard />
    </div>
  );
}

export default App;
